extends TabContainer


func OnFpsToggled(button_pressed):
	$ViewerFPS.visible = button_pressed

func OnFullToggled(button_pressed):
	OS.window_fullscreen = button_pressed

func OnSdPressed():
	OS.set_window_size(Vector2(1024,600))

func OnHdPressed():
	OS.set_window_size(Vector2(1280,720))

func OnFullHdPressed():
	OS.set_window_size(Vector2(1920,1080))
