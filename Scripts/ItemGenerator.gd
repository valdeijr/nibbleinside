extends Node2D

var maxItem
var itemAmount
var rng = RandomNumberGenerator.new()
var itemBase = preload("res://Instantiables/Item.tscn")

func _init():
	rng.randomize()

func StartGenerator(_typeItem, _maxItem):
	itemAmount = rng.randi_range(0,_maxItem)
	print(itemAmount)
	if itemAmount > 0:
		for _x in range(itemAmount):
			var newItem = itemBase.instance()
			newItem.init(_typeItem, self.position)
			get_parent().add_child(newItem)
		self.queue_free()
	self.queue_free()
