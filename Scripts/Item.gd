extends RigidBody2D

enum {
	FISH,
	WOOD,
	FOOD,
	SNIPPET_OF_TECH_RED,
	SNIPPET_OF_TECH_BLUE
	}

var typeItem
var rng = RandomNumberGenerator.new()
var _wear = 100 setget SetWear
func SetWear(value):
	_wear = value

func init(_type, _position):
	self.position = _position
	typeItem = _type

func _ready():
	rng.randomize()
	var impulse = Vector2(rng.randi_range(-100,100), rng.randi_range(-80,0))
	self.apply_central_impulse(impulse)
	itemType()
	yield(get_tree().create_timer(.35), "timeout")
	self.gravity_scale = 0
	$Area2D/CollisionShape2D.disabled = false
	yield($Sprite/AnimationPlayer, "animation_finished")
	#self.queue_free()

func itemType():
	match typeItem:
		"Fish":
			$Sprite.frame = FISH
		"Wood":
			$Sprite.frame = WOOD
		"Food":
			$Sprite.frame = FOOD
		"SotRed":
			$Sprite.frame = SNIPPET_OF_TECH_RED
		"SotRed":
			$Sprite.frame = SNIPPET_OF_TECH_BLUE

func OnArea2dEntered(body):
	if body.name == "Player":
		self.queue_free()
