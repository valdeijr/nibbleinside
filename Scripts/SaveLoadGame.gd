extends Button

export(NodePath) var game_node
export(NodePath) var player_node
#export(NodePath) onready var some_node = get_node_or_null(some_node if is_instance_valid(some_node) else "") as TypeOfNode
const SAVE_PATH = "user://save_config_file.ini"

func SaveGame():
	var config = ConfigFile.new()
	var player = get_node(player_node)
	config.set_value("player", "position", player.position)
	config.set_value("player", "health", player._health)
	var enemies = []
	for enemy in get_tree().get_nodes_in_group("Enemy"):
		enemies.push_back({
			position = enemy.position,
		})
	config.set_value("enemies", "enemies", enemies)
	config.save(SAVE_PATH)
	get_node("../LoadConfigFile").disabled = false #inCaseToggleButton


func LoadGame():
	var config = ConfigFile.new()
	config.load(SAVE_PATH)
	var player = get_node(player_node)
	player.position = config.get_value("player", "position")
	player._health = config.get_value("player", "health")
	# Remove existing and add new.
	for enemy in get_tree().get_nodes_in_group("Enemy"):
		enemy.queue_free()
	var enemies = config.get_value("enemies", "enemies")
	var game = get_node(game_node)
	for enemy_config in enemies:
		var enemy = preload("res://Instantiables/Galogino.tscn").instance()
		enemy.position = enemy_config.position
		game.add_child(enemy)


