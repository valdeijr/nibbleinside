var maxItem
var itemAmount
var itens = []
var rng = RandomNumberGenerator.new()
var itemBase = load("res://Instantiables/Item.tscn")

func _init():
	rng.randomize()

func StartGenerator(_typeItem, _maxItem):
	itemAmount = rng.randi_range(0,_maxItem)
	
	print(itemAmount)
	if itemAmount > 0:
		for _x in range(itemAmount):
			itens[_x] = itemBase.instance().init(_typeItem)
		return itens
	pass
