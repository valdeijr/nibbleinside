extends Control
func _ready():
	var currentSize = OS.window_size
	$CountFPS.rect_position = Vector2(currentSize.x - 35, 10)
func _process(_delta):
	$CountFPS.text = str(Engine.get_frames_per_second())
