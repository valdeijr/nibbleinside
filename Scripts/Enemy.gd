extends KinematicBody2D

var player
var isMove
var distanceTo
var _speed
var moveSeed
var _attack
var angle
var _angle = 0
var attack = false
var idle = true
var speed = 120
var rng = RandomNumberGenerator.new()
var _health = 100 setget SetHealth


func _ready():
	player = get_parent().find_node("Player")
	rng.randomize()


func _process(_delta):
	##### Calculo distancia e posição #####
	distanceTo = player.position - self.position
	distanceTo = sqrt(pow(distanceTo.x,2)+pow(distanceTo.y,2))
	##### Controle de movimento #####
	if distanceTo < 26:
		player._health -= TakeDamage() * _delta
	elif distanceTo < 120:
		_angle = get_angle_to(player.position)
		_speed = speed
	else:
		_speed = 0
		if !idle and moveSeed == 1:
			_speed = speed
			_angle = angle
	##### Aplicação do movimento #####
	isMove = move_and_collide(Vector2(1,0).rotated(_angle) * _delta * _speed)
	##### Verifica attack do Player #####
	if isMove and isMove.collider.name == "Player" and player.get_child(0).animation == "attack":
		if player.get_child(0).frame == 4:
			attack = true
		else:
			attack = false
		if attack:
			SetHealth(_health - (TakeDamage() + 20))
	##### Verifica a vida #####
	if _health <= 0:
		DropItens("Food", 2)

func OnTimerTimeout():
	angle = rng.randf_range(-3.14,3.14)
	moveSeed = rng.randi_range(1,3)
	if !idle:
		idle = true
	else:
		idle = false

func TakeDamage():
	return rng.randi_range(5,10)
	
func SetHealth(health):
	_health = health
	$LifeBar/TextureProgress.value = _health

func DropItens(_type, _max):
	var generator = load("res://Item.tscn").instance()
	generator.position = self.position
	get_parent().add_child(generator)
	generator.StartGenerator(_type, _max)
	queue_free()
