extends Node

var queue
var nextPath setget SetNextPath
func SetNextPath(value):
	nextPath = value

func _ready():
	# Initialize.
	queue = preload("res://Scripts/ResourceQueue.gd").new()
	# Call after you instance the class to start the thread.
	queue.start()
	set_process(true)
	# Queue a resource.
	queue.queue_resource(nextPath, true)

func _process(_delta):
	# Returns true if a resource is done loading and ready to be retrieved.
	if queue.is_ready(nextPath):
		set_process(false)
		# Returns the fully loaded resource.
		var next_scene = queue.get_resource(nextPath).instance()
		get_node("/root").add_child(next_scene)
		get_node("/root").remove_child(self)
		queue_free()
	else:
		# Get the progress of a resource.
		var progress = round(queue.get_progress(nextPath) * 100)
		get_node("ProgressBar").set_value(progress)
