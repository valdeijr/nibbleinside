extends KinematicBody2D

## Constants
const SPEED = 200

## Variables
var direction = Vector2()
var isMove = false
var isAttack = false
var isChange

## Properties
var _health = 100 setget SetHealth
func SetHealth(health):
	_health = health
	$LifeBar/TextureProgress.value = _health

func _physics_process(delta):
	if !isAttack:
		isMove = move_and_collide(direction * SPEED * delta)

func _process(_delta):
	InputActions()
	AnimatorPlayer()
	$Area2D/CollisionShape2D.disabled = isAttack

	##### Ação de Attack #####
	if Input.is_action_just_pressed("ui_action") and !isAttack:
		$AnimatedSprite.animation = "attack"
		if !$AudioStreamPlayer2D.playing:
			$AudioStreamPlayer2D.play()
		isAttack = true
	if isAttack and $AnimatedSprite.frame == 4:
		isAttack = false


func InputActions():
	var inputDirection = Vector2.ZERO
	inputDirection.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	inputDirection.y = Input.get_action_strength("ui_down")  - Input.get_action_strength("ui_up")
	if inputDirection != Vector2.ZERO:
		direction = inputDirection
		if inputDirection.x > 0:
			$AnimatedSprite.flip_h = false
		elif inputDirection.x < 0:
			$AnimatedSprite.flip_h = true
	else:
		direction = Vector2.ZERO
	

func AnimatorPlayer():
	if !isAttack:
		if direction.y != 0 and direction.x == 0:
			if direction.y < 0:
				$AnimatedSprite.animation = "walkBack"
			else:
				$AnimatedSprite.animation = "walkFront"
		elif direction.y != 0 or direction.x != 0:
			$AnimatedSprite.animation = "walk"
		else:
			$AnimatedSprite.animation = "idle"

func OnArea2dEntered(body):
	print(body)


#func _input(event):
	#if event is InputEventScreenTouch:
	#	print("touch")
	#elif event is InputEventKey or event is InputEventMouseButton:
	#	print("Computer")
	#elif event is InputEventJoypadButton or event is InputEventJoypadMotion:
	#	print("GamePad")


	##### Interações de colisões #####
	#f isMove and isMove.collider.name == "Porta":
	#	isChange = get_tree().change_scene("res://Scenes/Castle.tscn")
