extends Camera2D

func _process(_delta):
	if Input.is_action_just_released("ui_zoom_in") and self.zoom.x == 2 and self.zoom.y == 2:
		$AnimationPlayer.play("zoomIn")
		yield($AnimationPlayer, "animation_finished")
	if Input.is_action_just_released("ui_zoom_out") and self.zoom.x == 1 and self.zoom.y == 1:
		$AnimationPlayer.play("zoomOut")
		yield($AnimationPlayer, "animation_finished")
