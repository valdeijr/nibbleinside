extends Node

var currentScreenSize

func _ready():
	$Menu.visible = false
	OS.window_borderless = true
	currentScreenSize = OS.get_screen_size()

func _input(event):
	if (event is InputEventKey):
		OnAnimationFinished($SplashImage/AnimationPlayer)

func OnAnimationFinished(_anim_name):
	$Menu.visible = true
	$SplashImage.visible = false
	SetWindowSize(currentScreenSize)
	
func SetWindowSize(_size):
	OS.window_fullscreen = true
	OS.set_window_size(Vector2(_size.x,_size.y))
	
func PlayButtonPressed():
	var preloader = load("res://Scenes/Preloader.tscn").instance()
	preloader.nextPath = "res://Scenes/Game.tscn"
	get_node("/root").add_child(preloader)
	get_node("/root").remove_child(self)

func QuitButtonPressed():
	get_tree().quit()
	
